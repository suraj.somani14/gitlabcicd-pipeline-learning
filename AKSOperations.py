from azure.identity import ClientSecretCredential
from azure.mgmt.containerservice import ContainerServiceClient
from kubernetes import client, config
import yaml
import json
import os
import helper 
from exception_helper import handle_exception
from exception_helper import setup_logger
import configuration as GitlabConfigConnection

cicdGitlabConfig = GitlabConfigConnection.GitlabConfig

# setup logger file
loggerClient = setup_logger('InvoiceGenerator/Log/trace.log')

@handle_exception()
def GetAKSNamespaceDetails(): 
    print('inside GetAKSNamespaceDetails')    
    # get AKS client object 
    aks_client = GetAKSClient() 

    loggerClient.info('get AKS client object')

    # read JSON config file from secured file location
    pipeline_configuration = helper.ReadJSONFileFromCurrentDirectory(Filelocation="pipeline_configuration.json")
    
    loggerClient.info('read JSON config file from secured file location')

    # Get all clusters in the resource group
    clusters = pipeline_configuration[cicdGitlabConfig.NODE_AZURE_AKS_LIST]
    valueOfBillingConfig = pipeline_configuration[cicdGitlabConfig.NODE_VALUE_OF_BILLING]
    
    # Iterate over each cluster
    ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig)

@handle_exception()
def ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig):
    for cluster in clusters:
         # Initialize an empty list to store cluster and namespace details
        compline_namespaces_json = []
        raw_namespace_json =[]

        # Get the access profile for the cluster
        access_profile = aks_client.managed_clusters.list_cluster_user_credentials(
            os.environ.get(cicdGitlabConfig.VAR_AKS_RESOURCE_GROUP),
            cluster[cicdGitlabConfig.NODE_AKS_NAME] 
        )
        # # Extract the kubeconfig file content from the access profile
        kube_config_bytes = access_profile.kubeconfigs[0].value
        kube_config_str = kube_config_bytes.decode("utf-8")
        kube_config = yaml.safe_load(kube_config_str)

        configfile = "customkubeconfig"
        loggerClient.info('with open(configfile) Start ')
        with open(configfile, 'w') as file:
            json.dump(kube_config, file)
        loggerClient.info('with open(configfile) End ')
        # Define the command
        loggerClient.info('subprocess Start ')
        print(os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_ID))
        print(os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_SECRET))
        print(os.path(configfile))
        import subprocess
        command = [
            "kubelogin",
            "convert-kubeconfig",
            "-l",
            "spn",
            "--client-id",
            {os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_ID)},
            "--client-secret",
            {os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_SECRET)},
            "--kubeconfig",
            {configfile}
        ]
        # Execute the command
        subprocess.run(command, check=True)
        loggerClient.info('subprocess end ')
        # Load kubeconfig directly from dictionary
        config.load_kube_config(config_file=configfile)
#        config.kube_config.load_kube_config_from_dict(kube_config)
        # Create the API client
        k8s_client = client.CoreV1Api()

        # Get the list of namespaces for the cluster
        namespaces = k8s_client.list_namespace().items
        loggerClient.info('Get the list of namespaces for the cluster')
        # Raw namespace json file received as response from azure
        for rawNamespace in namespaces:
            rawNamespace_details = {
                'Cluster_Name': cluster[cicdGitlabConfig.NODE_AKS_NAME],
                'Namespace_Name': rawNamespace.metadata.name,
                'Namespace_Labels': rawNamespace.metadata.labels
            }
            raw_namespace_json.append(rawNamespace_details)
        
        raw_output_file = "InvoiceGenerator/RawData/" + cicdGitlabConfig.RAW_NAMESPACE_FILE_NAME.format(cluster[cicdGitlabConfig.NODE_AKS_NAME])
        #raw_output_file = "raw_"+cluster[config.NODE_AKS_NAME]+'.json'
        with open(raw_output_file, 'w') as file:
            json.dump(raw_namespace_json, file)
        
        # Compile namespaces data based on billing flag
        filterNamespaces = [namespace for namespace in namespaces if cicdGitlabConfig.LABEL_BILLING_FLAG in namespace.metadata.labels and namespace.metadata.labels["BillingFlag"] == 'true']

        # Iterate over each namespace and extract details
        for namespace in filterNamespaces:
            namespace_details = {
                'Cluster_Name': cluster[cicdGitlabConfig.NODE_AKS_NAME],
                'Namespace_Name': namespace.metadata.name,
                'Namespace_Labels': namespace.metadata.labels,
                'Value_Of_Billing': valueOfBillingConfig.get(namespace.metadata.labels[cicdGitlabConfig.LABEL_VALUE_OF_BILLING])
            }

            compline_namespaces_json.append(namespace_details)
        
        output_file = "InvoiceGenerator/CompileData/" + cluster[cicdGitlabConfig.NODE_AKS_NAME]+'.json'
        with open(output_file, 'w') as file:
            json.dump(compline_namespaces_json, file)
        
@handle_exception()
def GetAKSClient():
    # Authenticate using service principal credentials
    credentials = ClientSecretCredential(
        tenant_id=os.environ.get(cicdGitlabConfig.VAR_AZURE_TENANT_ID),
        client_id=os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_ID),
        client_secret=os.environ.get(cicdGitlabConfig.VAR_AZURE_CLIENT_SECRET)
    )
    
    # Instantiate the AKS client
    aks_client = ContainerServiceClient(credentials, os.environ.get(cicdGitlabConfig.VAR_AZURE_SUBSCRIPTION_ID))
    return aks_client

def main():
    print('inside main')
    GetAKSNamespaceDetails() 

if __name__ == "__main__":
    main()

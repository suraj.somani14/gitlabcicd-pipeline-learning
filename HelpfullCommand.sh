
export AZURE_TENANT_ID="e85feadf-11e7-47bb-a160-43b98dcc96f1"
export AZURE_CLIENT_ID="f84f60ba-c6a9-4eaa-9f33-95901ff99de5"
export AZURE_CLIENT_SECRET="Qq18Q~BUjDLm6ei2PL3VjkIXbNAOwm~DCkgFgbB9"
export AZURE_SUBSCRIPTION_ID="90a13e75-e07a-41b0-9438-4c3950cc72d3"
export AZURE_RESOURCE_GROUP="dscp-unit-test-dev-japaneast-rg"
export KUBECONFIG="/Users/admin/.kube/kubeconfig"

unset AZURE_TENANT_ID
unset AZURE_CLIENT_ID
unset AZURE_CLIENT_SECRET
unset AZURE_SUBSCRIPTION_ID
unset AZURE_RESOURCE_GROUP
unset KUBECONFIG

kubelogin convert-kubeconfig -l spn --client-id f84f60ba-c6a9-4eaa-9f33-95901ff99de5 --client-secret Qq18Q~BUjDLm6ei2PL3VjkIXbNAOwm~DCkgFgbB9

az role assignment create --role "Azure Kubernetes Service RBAC Reader" --scope /subscriptions/90a13e75-e07a-41b0-9438-4c3950cc72d3/resourcegroups/dscp-unit-test-dev-japaneast-rg/providers/Microsoft.ContainerService/managedClusters/run-cluster-japaneast-aks-local --assignee 05d03d25-855c-432b-ae6b-38ead3d97ea7

az role assignment create --role "Azure Kubernetes Service Cluster User Role" --scope /subscriptions/90a13e75-e07a-41b0-9438-4c3950cc72d3/resourcegroups/dscp-unit-test-dev-japaneast-rg/providers/Microsoft.ContainerService/managedClusters/run-cluster-japaneast-aks-local --assignee 05d03d25-855c-432b-ae6b-38ead3d97ea7

tenant_Id = ""
client_Id = ""
client_Secret=""

# Instantiate gitlab client
    gl = gitlab.Gitlab('https://gitlab.com/', private_token='glpat-8QdZBYFgo5G48TxuhdzX')

    # Retrieve the project ID where the environment variables are stored
    project_id = 46469653

    # Retrieve the environment variables for the specified project
    project = gl.projects.get(project_id)
    variables = project.variables.list()

    # Retrieve environment variables with default values in case they are not found
    tenant_Id = next((var for var in variables if var.key == 'AZURE_TENANT_ID'), None)
    client_Id = next((var for var in variables if var.key == 'AZURE_CLIENT_ID'), None)
    client_Secret = next((var for var in variables if var.key == 'AZURE_CLIENT_SECRET'), None)
    azure_SubscriptionId = next((var for var in variables if var.key == 'AZURE_SUBSCRIPTION_ID'), None)
    
     # Extract values from the variables or set default values if not found
    tenant_Id = tenant_Id.value if tenant_Id else 'default_tenant_id'
    client_Id = client_Id.value if client_Id else 'default_client_id'
    client_Secret = client_Secret.value if client_Secret else 'default_client_secret'
    azure_SubscriptionId = azure_SubscriptionId.value if azure_SubscriptionId else 'default_subscription_id'

    # Authenticate using service principal credentials
    credentials = ClientSecretCredential(
        tenant_id=tenant_Id,
        client_id=client_Id,
        client_secret=client_Secret
    ) 

from azure.identity import ClientSecretCredential
from azure.mgmt.containerservice import ContainerServiceClient
from kubernetes import client, config
import yaml
import json
import helper 
import configuration_local as LocalConfigConnection

cicdLocalConfig = LocalConfigConnection.LocalConfig

def GetAKSNamepsaceDetails():    

    # get AKS client object 
    aks_client = GetAKSClient() 

    #read JSON config file from secured file location
    pipeline_configuration = helper.ReadJSONFile(Filelocation=cicdLocalConfig.VAR_SECURE_CLUSTER_CONFIG_FILE_LOCATION)
    
    # Get all clusters in the resource group
    clusters = pipeline_configuration[cicdLocalConfig.NODE_AZURE_AKS_LIST]
    valueOfBillingConfig = pipeline_configuration[cicdLocalConfig.NODE_VALUE_OF_BILLING]
    
    # Iterate over each cluster
    ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig)


def ExtractAllNamespacesAndLabels(aks_client, clusters, valueOfBillingConfig):
    for cluster in clusters:
         # Initialize an empty list to store cluster and namespace details
        compline_namespaces_json = []
        raw_namespace_json =[]

        # Get the access profile for the cluster
        access_profile = aks_client.managed_clusters.list_cluster_user_credentials(
            cicdLocalConfig.VAR_AKS_RESOURCE_GROUP,
             cluster[cicdLocalConfig.NODE_AKS_NAME] 
        )
        # # Extract the kubeconfig file content from the access profile
        kube_config_bytes = access_profile.kubeconfigs[0].value
        kube_config_str = kube_config_bytes.decode("utf-8")
        kube_config = yaml.safe_load(kube_config_str)

        configfile = "customkubeconfig"
        with open(configfile, 'w') as file:
            json.dump(kube_config, file)
        
        # Define the command
        import subprocess
        command = [
            "kubelogin",
            "convert-kubeconfig",
            "-l",
            "spn",
            "--client-id",
            "f84f60ba-c6a9-4eaa-9f33-95901ff99de5",
            "--client-secret",
            "Qq18Q~BUjDLm6ei2PL3VjkIXbNAOwm~DCkgFgbB9",
            "--kubeconfig",
            configfile
        ]
        # Execute the command
        subprocess.run(command, check=True)
        
        # Load kubeconfig directly from dictionary
        config.load_kube_config(config_file=configfile)
#        config.kube_config.load_kube_config_from_dict(kube_config)
        # Create the API client
        k8s_client = client.CoreV1Api()

         
        # Get the list of namespaces for the cluster
        namespaces = k8s_client.list_namespace().items

        # Raw namespace json file received as response from azure
        for rawNamespace in namespaces:
            rawNamespace_details = {
                'Cluster_Name': cluster[cicdLocalConfig.NODE_AKS_NAME],
                'Namespace_Name': rawNamespace.metadata.name,
                'Namespace_Labels': rawNamespace.metadata.labels
            }
            raw_namespace_json.append(rawNamespace_details)
            
        raw_output_file = cicdLocalConfig.RAW_NAMESPACE_FILE_NAME.format(cluster[cicdLocalConfig.NODE_AKS_NAME])
        #raw_output_file = "raw_"+cluster[config.NODE_AKS_NAME]+'.json'
        with open(raw_output_file, 'w') as file:
            json.dump(raw_namespace_json, file)
        print(f"Raw Namespaces JSON saved to file: {raw_output_file}")
        # Print the namespaces in JSON format
        print("Raw Namespaces (JSON format):")
        print(raw_namespace_json)
        print(f"\nRaw Print Namespaces JSON saved file: {raw_output_file}")

        # Compile namespaces data based on billing flag
        filterNamespaces = [namespace for namespace in namespaces if cicdLocalConfig.LABEL_BILLING_FLAG in namespace.metadata.labels and namespace.metadata.labels["BillingFlag"] == 'true']

        # Iterate over each namespace and extract details
        for namespace in filterNamespaces:
            namespace_details = {
                'Cluster_Name': cluster[cicdLocalConfig.NODE_AKS_NAME],
                'Namespace_Name': namespace.metadata.name,
                'Namespace_Labels': namespace.metadata.labels,
                'Value_Of_Billing': valueOfBillingConfig.get(namespace.metadata.labels[cicdLocalConfig.LABEL_VALUE_OF_BILLING])
            }

            compline_namespaces_json.append(namespace_details)
        
        output_file = cluster[cicdLocalConfig.NODE_AKS_NAME]+'.json'
        with open(output_file, 'w') as file:
            json.dump(compline_namespaces_json, file)
        print(f"Namespaces JSON saved to file: {output_file}")
        # Print the namespaces in JSON format
        print("Namespaces (JSON format):")
        print(compline_namespaces_json)
        print(f"\nPrint Namespaces JSON saved file: {output_file}")



def GetAKSClient():
    # Authenticate using service principal credentials
    credentials = ClientSecretCredential(
        tenant_id=cicdLocalConfig.VAR_AZURE_TENANT_ID,
        client_id=cicdLocalConfig.VAR_AZURE_CLIENT_ID,
        client_secret=cicdLocalConfig.VAR_AZURE_CLIENT_SECRET
    )
    
    # Instantiate the AKS client
    aks_client = ContainerServiceClient(credentials, cicdLocalConfig.VAR_AZURE_SUBSCRIPTION_ID)
    return aks_client

def main():
    GetAKSNamepsaceDetails(); 

# x-ms-original-file: specification/containerservice/resource-manager/Microsoft.ContainerService/aks/stable/2023-04-01/examples/RunCommandRequest.json
if __name__ == "__main__":
    main()
